---
layout: page
title: Modeling of radiative heat-exchange using finite element method
description: A finite element method based solver for a mathematical model of heat-transfer comprised of a coupled system of conduction and radiation.
importance: 3
category: university
# redirect: https://gitlab.com/mukund-yedunuthala/ppp-ss2022
---


- This is a part of **Personal Programming Project (PPP)** coursework in _Computational Materials Science (CMS) M.Sc._ course in _Technische Universität Bergakademie Freiberg._ 

- Submitted in Summer Semester of 2022

- Documentation available at: [mukund-yedunuthala.gitlab.io/ppp-ss2022/](https://mukund-yedunuthala.gitlab.io/ppp-ss2022/)

- Repository: [GitLab](https://gitlab.com/mukund-yedunuthala/ppp-ss2022)