---
layout: page
title: Image convolution using Message Passing Interface (MPI)
description: Application of convolution kernels such as sharpening, blurring, edge detection to grayscale images in parallel using MPI.
importance: 3
category: university
# redirect: https://gitlab.com/mukund-yedunuthala/ppp-ss2022
---

- This is a part of **High performance computing and optimization** coursework in _Computational Materials Science (CMS) M.Sc._ course in _Technische Universität Bergakademie Freiberg._ 

- Documentation available at: [https://mukund-yedunuthala.gitlab.io/hpc-img-convolution/](https://mukund-yedunuthala.gitlab.io/hpc-img-convolution/)

- Repository: [GitLab](https://gitlab.com/mukund-yedunuthala/hpc-img-convolution)