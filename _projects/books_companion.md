---
layout: page
title: Books Companion
description: (WIP) Books companion is an Android application to track books, either to read or done reading. It is built entirely using Jetpack Compose and is in early stages of development. It is primarily intended to be a hobby project. 
importance: 4
category: misc
img: /assets/img/bc-thumbnail.jpg
redirect: https://github.com/mukund-yedunuthala/BooksCompanionApp
---