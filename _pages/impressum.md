---
layout: page
permalink: /impressum/
title: impressum
description: Impressum.
nav: false
nav_order: 4
---
**Angaben gemäß § 5 TMG:**

    Venkata Mukund Kashyap Yedunuthala
    Halsbrücker Straße 12
    09599 Freiberg
    Deutschland

**Kontakt:**

E-Mail: mukund.yedunuthala@outlook.de

**Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV:**

    Venkata Mukund Kashyap Yedunuthala
    Halsbrücker Straße 12
    09599 Freiberg
    Deutschland

**Haftungsausschluss:**

Haftung für Inhalte
Die Inhalte meiner Website wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und Aktualität der Inhalte kann ich jedoch keine Gewähr übernehmen.

Haftung für Links
Meine Website enthält Links zu externen Webseiten Dritter, auf deren Inhalte ich keinen Einfluss habe. Deshalb kann ich für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich.

Datenschutz
Die Nutzung meiner Website ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf meinen Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder E-Mail-Adresse) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Die Daten werden ohne ausdrückliche Zustimmung nicht an Dritte weitergegeben.