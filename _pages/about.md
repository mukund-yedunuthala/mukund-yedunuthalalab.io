---
layout: about
title: about
permalink: /
subtitle: Student | Technische Universität Bergakademie Freiberg

profile:
  align: right
  image: prof_pic.jpg
  image_circular: false # crops the image to make it circular
  # address:

news: false  # includes a list of news items
selected_papers: false # includes a list of papers marked as "selected={true}"
social: true # includes social icons at the bottom of the page
---
I am currently pursuing a Master's degree in Computational Materials Science at the TU Bergakademie Freiberg in Freiberg, Germany. Originally from Hyderabad, Telangana, India, I have been residing in Germany since 2019. I have a strong command of both German and English, allowing me to effectively communicate in bilingual settings.

In addition to my academic pursuits, I have diverse interests and hobbies. I consider myself a tech enthusiast and enjoy casual gaming. While I may struggle to meet my reading goals for the year, I compensate by exploring my passion for amateur photography.

Professionally, I am driven by a desire to immerse myself in problem-solving and hands-on work. My research interests focus on high-performance computing, finite element analysis, material modeling, and structural mechanics simulations. For a comprehensive overview of my qualifications, you can refer to my detailed CV, available in both English [here](assets/pdf/CV_Venkata-Mukund-Kashyap_Yedunuthala.pdf) and German [here](assets/pdf/Lebenslauf_Venkata-Mukund-Kashyap_Yedunuthala.pdf).

If you are interested in a more casual and informal account of my experiences, I maintain a personal blog, which can be found [here](https://kathalubymukund.wordpress.com/). Feel free to explore and connect with me there!
