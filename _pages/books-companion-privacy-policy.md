---
layout: page
permalink: /books-companion-privacy-policy/
title: Privacy Policy for Books Companion
description: Privacy Policy for Books Companion.
nav: false
---

Effective Date: [2023-05-19]

Thank you for using Books Companion. This Privacy Policy explains our approach to data collection and usage in relation to your use of our app.

## 1. Information Collection and Use

We do not collect any personal or non-personal information from users of our app. We do not utilize any trackers, cookies, or analytics tools to gather data.

## 2. Information Sharing and Disclosure

Since we do not collect any information, we do not share or disclose any data with third-party service providers or for any other purposes.

## 3. Data Security

Although we do not collect data, we still take reasonable measures to ensure the security of our app. This includes implementing industry-standard security practices to protect your privacy and app functionality.

## 4. Children's Privacy

Our app does not collect any information from users, including children under the age of 13.

## 5. Changes to This Privacy Policy

We reserve the right to update or modify this Privacy Policy from time to time. Any changes will be effective upon posting the revised policy in the app. By continuing to use our app after any changes, you acknowledge and agree to the revised Privacy Policy.

## 6. Contact Us

If you have any questions or concerns about this Privacy Policy, please contact us at [mukundyedunuthala@gmail.com](mailto:mukundyedunuthala@gmail.com) or using [matrix](https://matrix.to/#/@user:mukund-yedunuthala).

Please note that this Privacy Policy applies solely to the use of our app and does not cover any information collected by third-party services or websites linked to or from our app.
